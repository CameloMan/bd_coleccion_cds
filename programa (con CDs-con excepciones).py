from clases.genero  import *
################################################################
from db.admingenero import *
################################################################
from clases.discografica import *
################################################################
from db.admindiscografica import *
################################################################
from clases.cd  import *
################################################################
from db.admincd import *
################################################################
from clases.genero_cd  import *
################################################################

salir_opcion = 0

while salir_opcion != 1:

    opcion = int(input("\nSeleccione una opción:\n1. Género\n2. Discográfica\n3. CD\n0. Salir\n"))
    salir_accion = 0

    while salir_accion != 1 and opcion !=0:

        ####GÉNERO
        if(opcion==1):#OPCION==1 -> GÉNERO
            accion = int(input("\nSeleccione qué acción hacer con Género:\n1. Insertar\n2. Actualizar\n3. Eliminar\n4. Listar\n0. Salir\n"))
        
            if(accion==1):#ACCION==1 -> INSERT

                while True:#Dentro del try se hace el INSERT, sólo si los valores introducidos no están duplicados
                    try:                
                        genero = Genero(int(input("Introducir nuevo Id_Genero: ")), str(input("Introducir nuevo Nombre genero: ")))
                        admin = AdminGenero()
                        admin.InsertarGenero(genero)#Con un valor duplicado, el programa peta en esta instrucción
                        admin.CerrarConexionServidor()

                        salir_accion = 0
                        
                        break

                    except mysql.connector.errors.IntegrityError:
                        print("ERROR. El Id o el Nombre introducidos ya existen. Por favor, vuelva a intentarlo.\n")
                

            if(accion==2):#ACCION==2 -> UPDATE
                genero = Genero(int(input("Introducir Id_Genero a Actualizar: ")), str(input("Introducir nuevo Nombre genero: ")))
    
                admin = AdminGenero()
                admin.ActualizarGenero(genero)
                admin.CerrarConexionServidor()

                salir_accion = 0


            if(accion==3):#ACCION==3 -> DELETE

                while True:
                    try:
                        genero = Genero(int(input("Introducir Id_Genero a Eliminar: ")), "")
    
                        admin = AdminGenero()
                        admin.EliminarGenero(genero)
                        admin.CerrarConexionServidor()

                        salir_accion = 0

                        break

                    except mysql.connector.errors.IntegrityError:
                        print("ERROR. Este Id no se puede eliminar porque se encuentra en otra tabla. Por favor, vuelva a intentarlo.\n")


            if(accion==4):#ACCION==4 -> SELECT
                print("Los Generos de la BBDD son: ")
    
                admin = AdminGenero()
                lista_generos = admin.ListarGenero()

                for genero in lista_generos:
                    print('Id_Genero: '+str(genero.GetId_Genero()) + '  Nombre: ' + str(genero.GetGenero()))
                admin.CerrarConexionServidor()

                salir_accion = 0


            if(accion==0):#ACCION==0 -> VOLVER AL MENÚ
                print("Volviendo al menú principal...")
                salir_accion = 1
    

        ####DISCOGRÁFICA
        if(opcion==2):#OPCION==2 -> DISCOGRÁFICA
            accion = int(input("\nSeleccione qué acción hacer con Discográfica:\n1. Insertar\n2. Actualizar\n3. Eliminar\n4. Listar\n0. Salir\n"))

            if(accion==1):#ACCION==1 -> INSERT

                while True:#Dentro del try se hace el INSERT, sólo si los valores introducidos no están duplicados
                    try:
                        discografica = Discografica(int(input("Introducir nuevo Id_Discográfica: ")), str(input("Introducir nuevo Nombre discográfica: ")))
                        
                        admin = AdminDiscografica()
                        
                        admin.InsertarDiscografica(discografica)#Con un valor duplicado, el programa peta en esta instrucción
                        admin.CerrarConexionServidor()

                        salir_accion = 0
                        
                        break


                    except mysql.connector.errors.IntegrityError:
                        print("ERROR. El Id o el Nombre introducidos ya existen. Por favor, vuelva a intentarlo.\n")


            if(accion==2):#ACCION==2 -> UPDATE
                discografica = Discografica(int(input("Introducir Id_Discográfica a Actualizar: ")), str(input("Introducir nuevo Nombre discográfica: ")))
    
                admin = AdminDiscografica()
                admin.ActualizarDiscografica(discografica)
                admin.CerrarConexionServidor()

                salir_accion = 0


            if(accion==3):#ACCION==3 -> DELETE
                discografica = Discografica(int(input("Introducir Id_Discográfica a Eliminar: ")), "")
    
                admin = AdminDiscografica()
                admin.EliminarDiscografica(discografica)
                admin.CerrarConexionServidor()

                salir_accion = 0
            

            if(accion==4):#ACCION==4 -> SELECT
                print("Las Discográficas de la BBDD son: ")
    
                admin = AdminDiscografica()
                lista_discograficas = admin.ListarDiscografica()

                for discografica in lista_discograficas:
                    print('Id_Discografica: '+str(discografica.GetId_Discografica()) + '  Nombre: ' + str(discografica.GetDiscografica()))

                admin.CerrarConexionServidor()

                salir_accion = 0


            if(accion==0):#ACCION==0 -> VOLVER AL MENÚ
                print("Volviendo al menú principal...")
                salir_accion = 1


        ####CD
        if(opcion==3):#OPCION==3 -> CD
            accion = int(input("\nSeleccione qué acción hacer con CD:\n1. Insertar\n2. Actualizar\n3. Eliminar\n4. Listar\n0. Salir\n"))

            if(accion==1):#ACCION==1 -> INSERT

                while True:
                    try:
                        cd = CD(int(input("Introducir nuevo Id_Cd: ")), str(input("Introducir nuevo Nombre Cd: ")), str(input("Introducir Fecha compra (aaaa-mm-dd): ")), int(input("Introducir a qué Id_Discográfica pertenece el Cd: ")))
    
                        admin = AdminCD()
                        admin.InsertarCD(cd)
                        admin.CerrarConexionServidor()

                        salir_accion = 0

                        break

                    except mysql.connector.errors.IntegrityError:
                        print("ERROR. El Id o el Nombre introducidos ya existen. Por favor, vuelva a intentarlo.\n")


            if(accion==2):#ACCION==2 -> UPDATE

                accion_cd = input("\nQué quiere actualizar del CD:\n1. Actualizar CD\n2. Actualizar géneros del CD\n")

                if accion_cd == "1":

                    cd = CD(int(input("Introducir Id_Cd a Actualizar: ")), str(input("Introducir nuevo Nombre Cd: ")), str(input("Introducir nueva Fecha compra (aaaa-mm-dd): ")), int(input("Introducir nuevo Id_Discográfica a la que pertenece el Cd: ")))
        
                    admin = AdminCD()
                    admin.ActualizarCD(cd)
                    admin.CerrarConexionServidor()

                    salir_accion = 0


                if accion_cd == "2":
                    admin = AdminCD()
                    lista_cds = admin.ListarCD()

                    for cd in lista_cds:
                        print('Id_Cd: '+str(cd.GetId_Cd()) + '  Nombre: '+str(cd.GetNombre_Cd()) + ' Fecha Compra: '+str(cd.GetFecha_Compra()) + ' Id_Discografica: '+str(cd.GetId_Discografica()))


                    admin.CerrarConexionServidor()

                    cd_escogido = int(input("Introduzca de qué CD quiere actualizar el género: "))
                    print("1. Añadir género\n2. Eliminar género\n")
                    anadir_modificar_genero = input("Escoja la opción: ")

                    if anadir_modificar_genero == "1":
                        admin = AdminGenero()
                        lista_generos = admin.ListarGenero()
                        for genero in lista_generos:
                            print('Id_Genero: '+str(genero.GetId_Genero()) + '  Nombre: ' + str(genero.GetGenero()))
                        admin.CerrarConexionServidor()

                        anadir_genero = input("Ingrese Id_genero a Añadir: ")
                        admin = AdminCD()
                        admin.InsertarCD_Genero(cd_escogido,anadir_genero)

                    if anadir_modificar_genero == "2":
                        admin = AdminCD()
                        lista_generos_cd = admin.ListarGenero_CD(cd_escogido)
                        for genero in lista_generos_cd:
                            print('Id_Genero: '+str(genero.GetGenero()) + '  Nombre Género: ' + str(genero.GetCD()))
                        admin.CerrarConexionServidor()
                        eliminar_genero = input("Ingrese Id_genero a eliminar: ")
                        admin = AdminCD()
                        admin.EliminarCD_Genero(cd_escogido,eliminar_genero)


            if(accion==3):#ACCION==3 -> DELETE

                while True:
                    try:
                        cd = CD(int(input("Introducir Id_Discográfica a Eliminar: ")), "", "", "")
    
                        admin = AdminCD()
                        admin.EliminarCD(cd)
                        admin.CerrarConexionServidor()

                        salir_accion = 0

                        break

                    except mysql.connector.errors.IntegrityError:
                        print("ERROR. Este Id no se puede eliminar porque se encuentra en otra tabla. Por favor, vuelva a intentarlo.\n")


            if(accion==4):#ACCION==4 -> SELECT
                print("Los CDs de la BBDD son: ")
    
                admin = AdminCD()
                lista_cds = admin.ListarCD()

                for cd in lista_cds:
                    print('Id_Cd: '+str(cd.GetId_Cd()) + '  Nombre: '+str(cd.GetNombre_Cd()) + ' Fecha Compra: '+str(cd.GetFecha_Compra()) + ' Id_Discografica: '+str(cd.GetId_Discografica()))

                admin.CerrarConexionServidor()

                salir_accion = 0


            if(accion==0):#ACCION==0 -> VOLVER AL MENÚ
                print("Volviendo al menú principal...")
                salir_accion = 1


    if(opcion==0):#OPCION==0 -> SALIR DEL PROGRAMA
        print("Cerrando el programa...")
        salir_opcion = 1
