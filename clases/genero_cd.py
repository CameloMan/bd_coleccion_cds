class Genero_CD:
    def __init__(self, genero, cd):
        self.__genero = genero
        self.__cd = cd


    def SetGenero(self, genero):
        self.__genero = genero

    def GetGenero(self):
        return self.__genero


    def SetCD(self, cd):
        self.__cd = cd

    def GetCD(self):
        return self.__cd


    def __str__(self):
        return str(self.__genero)+" "+str(self.__cd)
