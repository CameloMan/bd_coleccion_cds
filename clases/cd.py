class CD:
    def __init__(self, id_cd, nombre, fecha_compra, id_discografica):
        self.__id_cd = id_cd
        self.__nombre = nombre
        self.__fecha_compra = fecha_compra
        self.__id_discografica = id_discografica


    def SetId_Cd(self, id_cd):
        self.__id_cd = id_cd

    def GetId_Cd(self):
        return self.__id_cd


    def SetNombre_Cd(self, nombre):
        self.__nombre = nombre

    def GetNombre_Cd(self):
        return self.__nombre


    def SetFecha_Compra(self, fecha_compra):
        self.__fecha_compra = fecha_compra

    def GetFecha_Compra(self):
        return self.__fecha_compra


    def SetId_Discografica(self, id_discografica):
        self.__id_discografica = id_discografica

    def GetId_Discografica(self):
        return self.__id_discografica


    def __str__(self):
        return str(self.__id_cd)+" "+str(self.__nombre)+" "+str(self.__fecha_compra)+" "+str(self.__id_discografica)
