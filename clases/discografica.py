class Discografica:
    def __init__(self, id_discografica, nombre):
        self.__id_discografica = id_discografica
        self.__nombre = nombre


    def SetId_Discografica(self, id_discografica):
        self.__id_discografica = id_discografica

    def GetId_Discografica(self):
        return self.__id_discografica


    def SetDiscografica(self, nombre):
        self.__nombre = nombre

    def GetDiscografica(self):
        return self.__nombre


    def __str__(self):
        return str(self.__id_discografica)+" "+str(self.__nombre)
