class Genero:
    def __init__(self, id_genero, nombre):
        self.__id_genero = id_genero
        self.__nombre = nombre


    def SetId_Genero(self, id_genero):
        self.__id_genero = id_genero

    def GetId_Genero(self):
        return self.__id_genero


    def SetGenero(self, nombre):
        self.__nombre = nombre

    def GetGenero(self):
        return self.__nombre


    def __str__(self):
        return str(self.__id_genero)+" "+str(self.__nombre)
