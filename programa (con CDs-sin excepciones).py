from clases.genero  import *
################################################################
from db.admingenero import *
################################################################
from clases.discografica import *
################################################################
from db.admindiscografica import *
################################################################
from clases.cd  import *
################################################################
from db.admincd import *
################################################################

salir_opcion = 0

while salir_opcion != 1:

    opcion = int(input("\nSeleccione una opción:\n1. Género\n2. Discográfica\n3. CD\n0. Salir\n"))
    salir_accion = 0

    while salir_accion != 1 and opcion !=0:

        ####GÉNERO
        if(opcion==1):#OPCION==1 -> GÉNERO
            accion = int(input("\nSeleccione qué acción hacer con Género:\n1. Insertar\n2. Actualizar\n3. Eliminar\n4. Listar\n0. Salir\n"))
        
            if(accion==1):#ACCION==1 -> INSERT
                genero = Genero(int(input("Introducir nuevo Id_Genero: ")), str(input("Introducir nuevo Nombre genero: ")))
    
                admin = AdminGenero()
                admin.InsertarGenero(genero)
                admin.CerrarConexionServidor()

                salir_accion = 0

            
            if(accion==2):#ACCION==2 -> UPDATE
                genero = Genero(int(input("Introducir Id_Genero a Actualizar: ")), str(input("Introducir nuevo Nombre genero: ")))
    
                admin = AdminGenero()
                admin.ActualizarGenero(genero)
                admin.CerrarConexionServidor()

                salir_accion = 0


            if(accion==3):#ACCION==3 -> DELETE
                genero = Genero(int(input("Introducir Id_Genero a Eliminar: ")), "")
    
                admin = AdminGenero()
                admin.EliminarGenero(genero)
                admin.CerrarConexionServidor()

                salir_accion = 0


            if(accion==4):#ACCION==4 -> SELECT
                print("Los Generos de la BBDD son: ")
    
                admin = AdminGenero()
                admin.ListarGenero()
                admin.CerrarConexionServidor()

                salir_accion = 0


            if(accion==0):#ACCION==0 -> VOLVER AL MENÚ
                print("Volviendo al menú principal...")
                salir_accion = 1
    

        ####DISCOGRÁFICA
        if(opcion==2):#OPCION==2 -> DISCOGRÁFICA
            accion = int(input("\nSeleccione qué acción hacer con Discográfica:\n1. Insertar\n2. Actualizar\n3. Eliminar\n4. Listar\n0. Salir\n"))

            if(accion==1):#ACCION==1 -> INSERT
                discografica = Discografica(int(input("Introducir nuevo Id_Discográfica: ")), str(input("Introducir nuevo Nombre discográfica: ")))
    
                admin = AdminDiscografica()
                admin.InsertarDiscografica(discografica)
                admin.CerrarConexionServidor()

                salir_accion = 0


            if(accion==2):#ACCION==2 -> UPDATE
                discografica = Discografica(int(input("Introducir Id_Discográfica a Actualizar: ")), str(input("Introducir nuevo Nombre discográfica: ")))
    
                admin = AdminDiscografica()
                admin.ActualizarDiscografica(discografica)
                admin.CerrarConexionServidor()

                salir_accion = 0


            if(accion==3):#ACCION==3 -> DELETE
                discografica = Discografica(int(input("Introducir Id_Discográfica a Eliminar: ")), "")
    
                admin = AdminDiscografica()
                admin.EliminarDiscografica(discografica)
                admin.CerrarConexionServidor()

                salir_accion = 0
            

            if(accion==4):#ACCION==4 -> SELECT
                print("Las Discográficas de la BBDD son: ")
    
                admin = AdminDiscografica()
                admin.ListarDiscografica()
                admin.CerrarConexionServidor()

                salir_accion = 0


            if(accion==0):#ACCION==0 -> VOLVER AL MENÚ
                print("Volviendo al menú principal...")
                salir_accion = 1


        ####CD
        if(opcion==3):#OPCION==3 -> CD
            accion = int(input("\nSeleccione qué acción hacer con CD:\n1. Insertar\n2. Actualizar\n3. Eliminar\n4. Listar\n0. Salir\n"))

            if(accion==1):#ACCION==1 -> INSERT
                cd = CD(int(input("Introducir nuevo Id_Cd: ")), str(input("Introducir nuevo Nombre Cd: ")), str(input("Introducir Fecha compra (aaaa-mm-dd): ")), int(input("Introducir a qué Id_Discográfica pertenece el Cd: ")))
    
                admin = AdminCD()
                admin.InsertarCD(cd)
                admin.CerrarConexionServidor()

                salir_accion = 0


            if(accion==4):#ACCION==4 -> SELECT
                print("Los CDs de la BBDD son: ")
    
                admin = AdminCD()
                admin.ListarCD()
                admin.CerrarConexionServidor()

                salir_accion = 0


            if(accion==0):#ACCION==0 -> VOLVER AL MENÚ
                print("Volviendo al menú principal...")
                salir_accion = 1


    if(opcion==0):#OPCION==0 -> SALIR DEL PROGRAMA
        print("Cerrando el programa...")
        salir_opcion = 1
