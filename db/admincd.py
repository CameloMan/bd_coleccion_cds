import mysql.connector
from clases.cd import *
from clases.genero_cd import *

class AdminCD:

    def __init__(self):

        self.__cnx = mysql.connector.connect(user='root', password='root', host='localhost', database='coleccion_cds')


    def InsertarCD(self, cd):
        
        cursor = self.__cnx.cursor()
        
        query = ("INSERT INTO cd (id_cd, titulo, fecha_compra, id_discografica) VALUES ('%i','%s','%s','%i')" % (cd.GetId_Cd(), cd.GetNombre_Cd(), cd.GetFecha_Compra(), cd.GetId_Discografica()))

        cursor.execute(query)
        self.__cnx.commit()
                            
        cursor.close()


    def InsertarCD_Genero(self, id_cd, id_genero):
        
        cursor = self.__cnx.cursor()
        
        query = ("INSERT INTO genero_cd (id_genero, id_cd) VALUES ('%s','%s')" % (id_genero, id_cd))

        cursor.execute(query)
        self.__cnx.commit()
                            
        cursor.close()



    def ActualizarCD(self, cd):

        cursor = self.__cnx.cursor()

        query = "UPDATE cd SET titulo = '%s',fecha_compra = '%s',id_discografica = '%s' WHERE id_cd = %i" % (cd.GetNombre_Cd(), cd.GetFecha_Compra(),cd.GetId_Discografica(),cd.GetId_Cd())

        cursor.execute(query)
        self.__cnx.commit()

        cursor.close()


    def EliminarCD(self, cd):

        cursor = self.__cnx.cursor()

        query = "DELETE FROM cd WHERE id_cd= %i" % (cd.GetId_Cd())

        cursor.execute(query)
        self.__cnx.commit()

        cursor.close()


    def ListarCD(self):

        cursor = self.__cnx.cursor()

        query = ('SELECT id_cd, titulo, fecha_compra, id_discografica FROM cd')

        cursor.execute(query)
        cds = cursor.fetchall()
        
        lista_cds = []
                
        for cd in cds:
            lista_cds.append(CD(cd[0],cd[1],cd[2],cd[3]))

        cursor.close()

        return lista_cds
        
        #Cada posició de la llista contindrà un objecte de clase Genero, compost
        #a la vegada per un id_genero (seria genero[0]), i un nombre (genero[1])
        #I.e. lista_generos[0] mostra -> 1 Jazz
        
        

    def ListarGenero_CD(self, id_cd):

            cursor = self.__cnx.cursor()

            query = ('SELECT g.id_genero, g.nombre FROM genero_cd gc JOIN cd c ON gc.id_cd = c.id_cd JOIN genero g ON g.id_genero = gc.id_genero WHERE c.id_cd = %s' % (id_cd))            


            cursor.execute(query)
            generos = cursor.fetchall()
            
            lista_generos = []
                    
            for genero in generos:
                lista_generos.append(Genero_CD(genero[0],genero[1]))

            cursor.close()

            return lista_generos


    def EliminarCD_Genero(self, id_cd,id_genero):

        cursor = self.__cnx.cursor()

        query = "DELETE FROM genero_cd WHERE id_cd= %s AND id_genero=%s"  % (id_cd,id_genero)

        cursor.execute(query)
        self.__cnx.commit()

        cursor.close()

                 
    def CerrarConexionServidor(self):
        self.__cnx.close()#Llamarla en el main, dentro de cada Accion (INSERT, UPDATE, DELETE, SELECT)
