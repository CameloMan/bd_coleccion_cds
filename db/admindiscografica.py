import mysql.connector
from clases.discografica import *

class AdminDiscografica:

    def __init__(self):

        self.__cnx = mysql.connector.connect(user='root', password='root', host='localhost', database='coleccion_cds')


    def InsertarDiscografica(self, discografica):
        
        cursor = self.__cnx.cursor()
        
        query = ("INSERT INTO discografica (id_discografica, nombre) VALUES ('%i','%s')" % (discografica.GetId_Discografica(), discografica.GetDiscografica()))

        cursor.execute(query)
        self.__cnx.commit()
                            
        cursor.close()


    def ActualizarDiscografica(self, discografica):

        cursor = self.__cnx.cursor()

        query = "UPDATE discografica SET nombre = '%s' WHERE id_discografica = %i" % (discografica.GetDiscografica(), discografica.GetId_Discografica())

        cursor.execute(query)
        self.__cnx.commit()
                            
        cursor.close()


    def EliminarDiscografica(self, discografica):

        cursor = self.__cnx.cursor()

        query = "DELETE FROM discografica WHERE id_discografica = %i" % (discografica.GetId_Discografica())

        cursor.execute(query)
        self.__cnx.commit()
                            
        cursor.close()


    def ListarDiscografica(self):

        cursor = self.__cnx.cursor()

        query = ('SELECT id_discografica, nombre FROM discografica')

        cursor.execute(query)
        discograficas = cursor.fetchall()

        lista_discograficas = []

        for discografica in discograficas:
            lista_discograficas.append(Discografica(discografica[0],discografica[1]))

        cursor.close()

        return lista_discograficas
        
        #Cada posició de la llista contindrà un objecte de clase Discografica, compost
        #a la vegada per un id_discografica (seria discografica[0]), i un nombre (discografica[1])
        #I.e. lista_discograficas[0] mostra -> 1 Sony Music
            
        
             
    def CerrarConexionServidor(self):
        self.__cnx.close()#Llamarla en el main, dentro de cada Accion (INSERT, UPDATE, DELETE, SELECT)
