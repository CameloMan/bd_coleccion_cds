import mysql.connector
from clases.genero import *

class AdminGenero:

    def __init__(self):

        self.__cnx = mysql.connector.connect(user='root', password='root', host='localhost', database='coleccion_cds')


    def InsertarGenero(self, genero):
        
        cursor = self.__cnx.cursor()
        
        query = ("INSERT INTO genero (id_genero, nombre) VALUES ('%i','%s')" % (genero.GetId_Genero(), genero.GetGenero()))

        cursor.execute(query)
        self.__cnx.commit()
                            
        cursor.close()


    def ActualizarGenero(self, genero):

        cursor = self.__cnx.cursor()

        query = "UPDATE genero SET nombre = '%s' WHERE id_genero = %i" % (genero.GetGenero(), genero.GetId_Genero())

        cursor.execute(query)
        self.__cnx.commit()
                            
        cursor.close()


    def EliminarGenero(self, genero):

        cursor = self.__cnx.cursor()

        query = "DELETE FROM genero WHERE id_genero = %i" % (genero.GetId_Genero())

        cursor.execute(query)
        self.__cnx.commit()
                            
        cursor.close()


    def ListarGenero(self):

        cursor = self.__cnx.cursor()

        query = ('SELECT id_genero, nombre FROM genero')

        cursor.execute(query)
        generos = cursor.fetchall()
        
        lista_generos = []
                
        for genero in generos:
            lista_generos.append(Genero(genero[0],genero[1]))

        cursor.close()

        return lista_generos
        
        #Cada posició de la llista contindrà un objecte de clase Genero, compost
        #a la vegada per un id_genero (seria genero[0]), i un nombre (genero[1])
        #I.e. lista_generos[0] mostra -> 1 Jazz
        
        
             
    def CerrarConexionServidor(self):
        self.__cnx.close()#Llamarla en el main, dentro de cada Accion (INSERT, UPDATE, DELETE, SELECT)
