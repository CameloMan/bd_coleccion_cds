CREATE DATABASE  IF NOT EXISTS `coleccion_cds` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `coleccion_cds`;
-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: coleccion_cds
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `autor`
--

DROP TABLE IF EXISTS `autor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `autor` (
  `id_autor` int(11) NOT NULL,
  `nombre_artistico` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_autor`),
  UNIQUE KEY `id_autor_UNIQUE` (`id_autor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autor`
--

LOCK TABLES `autor` WRITE;
/*!40000 ALTER TABLE `autor` DISABLE KEYS */;
INSERT INTO `autor` VALUES (1,'Lady Gaga'),(2,'Andrea Bocelli'),(3,'Rihanna');
/*!40000 ALTER TABLE `autor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autor_cd`
--

DROP TABLE IF EXISTS `autor_cd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `autor_cd` (
  `id_autor` int(11) NOT NULL,
  `id_cd` int(11) NOT NULL,
  PRIMARY KEY (`id_autor`,`id_cd`),
  KEY `FK_CD__AUTOR_CD_idx` (`id_cd`),
  CONSTRAINT `FK_AUTOR__AUTOR_CD` FOREIGN KEY (`id_autor`) REFERENCES `autor` (`id_autor`),
  CONSTRAINT `FK_CD__AUTOR_CD` FOREIGN KEY (`id_cd`) REFERENCES `cd` (`id_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autor_cd`
--

LOCK TABLES `autor_cd` WRITE;
/*!40000 ALTER TABLE `autor_cd` DISABLE KEYS */;
INSERT INTO `autor_cd` VALUES (2,1),(3,1),(2,2),(1,3),(3,3);
/*!40000 ALTER TABLE `autor_cd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd`
--

DROP TABLE IF EXISTS `cd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd` (
  `id_cd` int(11) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `fecha_compra` date DEFAULT NULL,
  `id_discografica` int(11) DEFAULT NULL,
  `id_idioma` int(11) DEFAULT NULL,
  `id_comprador` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_cd`),
  UNIQUE KEY `id_cd_UNIQUE` (`id_cd`),
  KEY `FK_CD_DISCOGRAFICA_idx` (`id_discografica`),
  KEY `FK_CD_IDIOMA_idx` (`id_idioma`),
  KEY `FK_CD_COMPRADOR_idx` (`id_comprador`),
  CONSTRAINT `FK_CD_COMPRADOR` FOREIGN KEY (`id_comprador`) REFERENCES `comprador` (`id_comprador`),
  CONSTRAINT `FK_CD_DISCOGRAFICA` FOREIGN KEY (`id_discografica`) REFERENCES `discografica` (`id_discografica`),
  CONSTRAINT `FK_CD_IDIOMA` FOREIGN KEY (`id_idioma`) REFERENCES `idioma` (`id_idioma`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd`
--

LOCK TABLES `cd` WRITE;
/*!40000 ALTER TABLE `cd` DISABLE KEYS */;
INSERT INTO `cd` VALUES (1,'Daylight','2018-04-30',1,2,3),(2,'Nightfall','2007-02-28',2,1,2),(3,'Sunrise','2010-06-01',1,3,2);
/*!40000 ALTER TABLE `cd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comprador`
--

DROP TABLE IF EXISTS `comprador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `comprador` (
  `id_comprador` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_comprador`),
  UNIQUE KEY `id_comprador_UNIQUE` (`id_comprador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comprador`
--

LOCK TABLES `comprador` WRITE;
/*!40000 ALTER TABLE `comprador` DISABLE KEYS */;
INSERT INTO `comprador` VALUES (1,'Luis','Gomez'),(2,'Andreu','Pla'),(3,'Pep','Mas');
/*!40000 ALTER TABLE `comprador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discografica`
--

DROP TABLE IF EXISTS `discografica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `discografica` (
  `id_discografica` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_discografica`),
  UNIQUE KEY `id_discografica_UNIQUE` (`id_discografica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discografica`
--

LOCK TABLES `discografica` WRITE;
/*!40000 ALTER TABLE `discografica` DISABLE KEYS */;
INSERT INTO `discografica` VALUES (1,'Sony Music'),(2,'Warner Music'),(3,'Columbia Records');
/*!40000 ALTER TABLE `discografica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genero`
--

DROP TABLE IF EXISTS `genero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `genero` (
  `id_genero` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_genero`),
  UNIQUE KEY `id_genero_UNIQUE` (`id_genero`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genero`
--

LOCK TABLES `genero` WRITE;
/*!40000 ALTER TABLE `genero` DISABLE KEYS */;
INSERT INTO `genero` VALUES (1,'Jazz'),(2,'Rock'),(3,'Fado');
/*!40000 ALTER TABLE `genero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genero_cd`
--

DROP TABLE IF EXISTS `genero_cd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `genero_cd` (
  `id_genero` int(11) NOT NULL,
  `id_cd` int(11) NOT NULL,
  PRIMARY KEY (`id_genero`,`id_cd`),
  KEY `FK_CD__GENERO_CD_idx` (`id_cd`),
  CONSTRAINT `FK_CD__GENERO_CD` FOREIGN KEY (`id_cd`) REFERENCES `cd` (`id_cd`),
  CONSTRAINT `FK_GENERO__GENERO_CD` FOREIGN KEY (`id_genero`) REFERENCES `genero` (`id_genero`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genero_cd`
--

LOCK TABLES `genero_cd` WRITE;
/*!40000 ALTER TABLE `genero_cd` DISABLE KEYS */;
INSERT INTO `genero_cd` VALUES (1,1),(2,1),(2,2),(3,2),(3,3);
/*!40000 ALTER TABLE `genero_cd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `idioma`
--

DROP TABLE IF EXISTS `idioma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `idioma` (
  `id_idioma` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_idioma`),
  UNIQUE KEY `id_idioma_UNIQUE` (`id_idioma`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `idioma`
--

LOCK TABLES `idioma` WRITE;
/*!40000 ALTER TABLE `idioma` DISABLE KEYS */;
INSERT INTO `idioma` VALUES (1,'Catalan'),(2,'Ingles'),(3,'Frances');
/*!40000 ALTER TABLE `idioma` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-12 13:56:07
